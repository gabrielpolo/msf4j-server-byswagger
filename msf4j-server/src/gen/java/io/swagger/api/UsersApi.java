package io.swagger.api;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.core.Response;

import io.swagger.api.factories.UsersApiServiceFactory;
import io.swagger.model.Users;

@Path("/users")


@io.swagger.annotations.Api(description = "the users API")
@javax.annotation.Generated(value = "io.swagger.codegen.languages.JavaMSF4JServerCodegen", date = "2018-09-29T19:53:09.027Z")
public class UsersApi  {
   private final UsersApiService delegate = UsersApiServiceFactory.getUsersApi();

    @GET  
    @io.swagger.annotations.ApiOperation(value = "Returns a list of users.", notes = "Returns all users registered in the system.", response = Users.class, tags={  })
    @io.swagger.annotations.ApiResponses(value = { 
        @io.swagger.annotations.ApiResponse(code = 200, message = "A JSON array of users", response = Users.class) })
    public Response usersGet()
    throws NotFoundException {
        return delegate.usersGet();
    }
}
